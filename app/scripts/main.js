var myMap,
    myCollection,
    myGeoObjects,
    myClusterer,
    userpin;

(function($) {
    "use strict";


    /*
     * Navigation
     **/
    function navigation() {
        $(".js-nav").each(function() {
            var handler = $(this);
            var menu = $(".nav-mobile");
            var overlay = $(".nav-mobile__overlay");

            $(handler).on("click", function(e) {
                e.stopPropagation();
                $("body").toggleClass("open-menu");
                menu.toggleClass("open");

                if($("body").hasClass("open-menu")) {
                    $("body").append("<div class='nav-mobile__overlay'></div>");
                    $(".nav-mobile__overlay").animate({"opacity": 1}, 300, function() {
                        $("html").addClass("is-not-scroll");
                    });
                } else {
                    $("body").find(".nav-mobile__overlay").animate({"opacity": 0}, 300, function() {
                        if($(this).css('opacity') == 0) {
                            $(this).remove();
                        }

                        $("html").removeClass("is-not-scroll");
                    });
                }
            });


            menu.on("click", function(e) {
                var inner = $(".nav-mobile__inner");

                if(!inner.is(e.target)) {
                    $("body").removeClass("open-menu");
                    menu.removeClass("open");
                    $("body").find(".nav-mobile__overlay").animate({"opacity": 0}, 300, function() {
                        if($(this).css('opacity') == 0) {
                            $(this).remove();
                        }
                        $("html").removeClass("is-not-scroll");
                    });
                }
            });

        });



        $(".nav-mobile").each(function() {
            var nav = $(this);
            var navtop = nav.find(".navtop");
            var link = navtop.find(".navtop__item.dropdown a.navtop__link");

            link.on("click", function() {
                var parent = $(this).parent();
                var subnav = parent.find("ul");
                parent.toggleClass("dropdown--open");

                if(parent.hasClass("dropdown--open")) {
                    subnav.slideDown(350);
                } else {
                    subnav.slideUp(350);
                }

                return false;
            });
        });
    }



    /*
     * Slider
     **/

    function InitializeSlider() {

        if (navigator.userAgent.indexOf("Safari")!=-1 && navigator.userAgent.indexOf("Chrome")==-1)   {
            //console.log('это сафари');
            $(".slider-for").slick({
                infinite: true,
                fade: true,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                swipe: true,
                prevArrow: '<button class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
                nextArrow: '<button class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
                asNavFor: '.slider-nav',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            fade: false,
                            centerMode: true,
                            variableWidth: true,
                            dots: true,
                            speed: 300
                        }
                    }
                ]
            });

            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                focusOnSelect: true,
                asNavFor: '.slider-for'
            });
        } else {
            $(".slider-for").slick({
                infinite: true,
                fade: true,
                cssEase: 'linear',
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                swipe: true,
                prevArrow: '<button class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
                nextArrow: '<button class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
                asNavFor: '.slider-nav',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            fade: false,
                            centerMode: true,
                            variableWidth: true,
                            dots: true,
                            speed: 300
                        }
                    }
                ]
            });

            $('.slider-nav').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                focusOnSelect: true,
                asNavFor: '.slider-for'
            });
        }
    }


    function zoomInitialize() {
        var zoom = {};

        $("[data-zoom]").each(function(i) {
            if($(this).data('zoom') == true) {
                zoom[i] = {
                    el: $(this),
                    image: $(this).find('img'),
                    thumb: $(this).find('img').data('thumb'),
                    viewport: $(this).append("<div class='zoom-viewport'></div>"),
                    button: $(this).append("<div class='zoom-button'><span class='zoom-button__plus'><svg class='icon icon-zoom' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><use xlink:href='../icons/icons.svg#zoom'></use></svg></span><span class='zoom-button__minus'><svg class='icon icon-zoom' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'><use xlink:href='../icons/icons.svg#zoom-minus'></use></svg></span></div>"),
                    btn: $(this).find(".zoom-button"),
                    touch: true,
                    touched: false
                };


                $(zoom[i].btn).find('.zoom-button__plus').on('click', function(e) {
                    e.preventDefault();

                    $(zoom[i].el).find('.zoom-viewport').append('<img src="' + zoom[i].thumb + '">');
                    if ($(zoom[i].el).find('.zoom-viewport').find('img').length) {
                        $(zoom[i].el).find('.zoom-viewport').css('display', 'block');
                        $(zoom[i].image).css('display', 'none');

                        $('.product-image').addClass('zoom-view');
                        $('.product-head').addClass('fixed-left');
                        $('.product-inner').addClass('fixed-left');
                    } else {
                        $(zoom[i].el).find('.zoom-viewport').css('display', 'none').removeAttr('style');
                        $(zoom[i].image).css('display', 'block');

                        $('.product-image').removeClass('zoom-view');
                        $('.product-head').removeClass('fixed-left');
                        $('.product-inner').removeClass('fixed-left');
                    }

                    $('.product-slider__item').trigger('resize');
                    $(window).trigger('resize');
                });


                $(zoom[i].btn).find('.zoom-button__minus').on('click', function(e) {
                    e.preventDefault();

                    $(zoom[i].image).css('display', 'block');
                    $('.zoom-viewport').css('display', 'none').removeAttr('style');
                    $('.zoom-viewport').find('img').remove();

                    $('.product-image').removeClass('zoom-view');
                    $('.product-head').removeClass('fixed-left');
                    $('.product-inner').removeClass('fixed-left');

                    $('.product-slider__item').trigger('resize');
                    $(window).trigger('resize');
                });
            }


            // переключение слайдов
            $('.slider-nav').on('click', '.slider-nav__item', function(e) {
                e.preventDefault();

                if($('.product-image').hasClass('zoom-view')) {
                    $('.product-slider').each(function () {
                        var slide = $(this).find('.product-slider__item.slick-active');
                        var thumb = slide.find('img').data('thumb');

                        if (slide) {
                            if($(zoom[i].el).find('.zoom-viewport').find('img').length) {
                                $(zoom[i].el).find('.zoom-viewport').find('img').attr('src', thumb);
                            } else {
                                $(zoom[i].image).css('display', 'none');
                                $(zoom[i].el).find('.zoom-viewport').css('display', 'block');
                                $(zoom[i].el).find('.zoom-viewport').append('<img src="' + zoom[i].thumb + '">');
                            }
                        }
                    });
                } else {
                    $(zoom[i].image).css('display', 'block');
                    $('.zoom-viewport').css('display', 'none').removeAttr('style');
                    $('.zoom-viewport').find('img').remove();
                }
            });
        });
    }




    /*
     * Quantity
     **/

    function quantity() {
        $("[data-id='quantity']").each(function() {
            var counter = $(this),
                input = counter.find('input[type=text]'),
                minus = counter.find("[data-quantity='minus']"),
                plus = counter.find("[data-quantity='plus']");

            if(input.prop('disabled') == true) {
                counter.addClass('disabled');
            }


            $(minus).click(function() {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 1 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $(plus).click(function() {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });

        });
    }



    /*
     * Accordion
     **/
    function initializeAccordions() {
        var accordions = {};
        $("[data-accordion]").each(function () {
            var $this = $(this);
            var tabitem = $this.find(".tab-content__item");
            var tabheading = tabitem.find(".tab-content__heading");
            var taboverlay = tabheading.find(".tab-content__overlay");

            if($(window).width() < 767) {
                if($(tabitem).hasClass("active")) {
                    $(".tab-content__item.active").removeClass("active");


                    if($(".tab-content__heading").hasClass("open")) {
                        $(".tab-content__heading.open").next().slideDown(350);
                    }
                }
            } else {
                $(".tab-content__item:first-child").addClass('active');
                $(".tab-content__heading").removeClass("open");
                $(".tab-content__overlay").removeAttr("style");
            }

            $(window).resize(function() {
                if($(window).width() < 767) {
                    if($(tabitem).hasClass("active")) {
                        $(".tab-content__item.active").removeClass("active");

                        if($(".tab-content__heading").hasClass("open")) {
                            $(".tab-content__heading.open").next().slideDown(350);
                        }
                    }
                } else {
                    $(".tab-content__item:first-child").addClass('active');
                    $(".tab-content__heading").removeClass("open");
                    $(".tab-content__overlay").removeAttr("style");
                }
            });


            if($(tabitem).hasClass("active")) {
                $(".tab-content__item.active").find(".tab-content__heading").addClass("open");

                if($(".tab-content__heading").hasClass("open")) {
                    $(".tab-content__heading.open").next().slideDown(350);
                }
            }


            $(document).on("click", "[data-accordion-tumbler]", function(i) {
                var accordion = $(this).parentsUntil("[data-accordion]").eq(0);
                var accordionID = i;

                accordions[accordionID] = {
                    accordion: accordion,
                    tumbler: $(this),
                    content: (accordion.find("[data-accordion-content]").length) ? accordion.find("[data-accordion-content]").first() : null
                };

                accordions[accordionID].tumbler.addClass('open')
                    .next().slideDown(350)
                    .parent().addClass("active")
                    .siblings().removeClass("active")
                    .children().removeClass("open")
                    .next().slideUp(350);
                //accordions[accordionID].content.slideToggle(350);

                return false;
            });


            $(document).on("click", ".open[data-accordion-tumbler]", function(i) {
                var accordion = $(this).parentsUntil("[data-accordion]").eq(0);
                var accordionID = i;

                accordions[accordionID] = {
                    accordion: accordion,
                    tumbler: $(this),
                    content: (accordion.find("[data-accordion-content]").length) ? accordion.find("[data-accordion-content]").first() : null
                };

                accordions[accordionID].tumbler.removeClass('open')
                    .next().slideUp(350)
                    .parent().removeClass("active")
                    .children().removeClass("open");
                //accordions[accordionID].content.slideToggle(350);

                return false;
            });
        });
    }


    /* Form validation */
    /*
     function formValidation(options) {
     var $form = $('form');
     //console.log($form);

     // settings
     var settings = $.extend({
     ignore: '',
     debug: false,
     errorElement: 'span',
     validClass: 'i-valid',
     errorClass: 'form__error',
     errorPlacement: function(error, element) {
     error.insertAfter(element);
     },
     highlight: function(element, errorClass, validClass) {
     $(element).removeClass('i-valid').addClass('i-error');
     },
     unhighlight: function(element, errorClass, validClass) {
     $(element).removeClass('i-error').addClass('i-valid');
     }
     }, options);


     // messages
     $.extend($.validator.messages, {
     required: "Вы не заполнили обязательное поле",
     remote: "Введите правильное значение",
     email: "Введите корректный email",
     url: "Введите корректный URL",
     date: "Введите корректную дату",
     dateISO: "Введите корректную дату в формате ISO",
     number: "Введите число",
     digits: "Вводите только цифры",
     creditcard: "Введите правильный номер кредитной карты",
     equalTo: "Введите такое же значение ещё раз",
     extension: "Выберите файл с правильным расширением",
     maxlength: $.validator.format("Введите не больше {0} символов"),
     minlength: $.validator.format("Введите не меньше {0} символов"),
     rangelength: $.validator.format("Введите значение длиной от {0} до {1} символов"),
     range: $.validator.format("Введите число от {0} до {1}"),
     max: $.validator.format("Введите число, меньшее или равное {0}"),
     min: $.validator.format("Введите число, большее или равное {0}")
     });

     // form init
     $form.validate(settings);
     }/**/


    /*
     * Table accordion
     * */
    function tableAccordion() {
        $('.table-accordion').each(function(i) {
            var table = $(this).eq(i),
                handle = $(table).find('.headline');


            $(window).resize(function() {
                if($(window).width() > 767) {
                    table.find('.description').removeAttr('style');
                    table.find('.active').removeClass('active');
                    table.find('.open').removeClass('open');
                }
            });


            handle.on('click', function (e) {
                e.preventDefault();
                var content = $(this).next('.description');
                $(this).parent().toggleClass('active');
                $(this).toggleClass('open');
                //$(content).slideDown(350);

                if ($(this).hasClass('open')) {
                    $(content).slideDown(350);
                } else {
                    $(content).slideUp(350);
                }
            });
        });
    }



    /*
     * Popup
     * */
    function popupInit() {
        $('[data-popup]').each(function(i) {
            var handler = $(this).eq(i),
                popup = $('#' + $(handler).data('popup')),
                close = $('[data-toggle="close"]');

            getPos();
            $(window).resize(function() {
                popup.css({
                    top: (getPos() - popup.outerHeight() + popup.outerHeight() + 30 + 'px')
                });
            });

            handler.on('click', function(e) {
                e.preventDefault();

                popup.css({
                    top: (getPos() - popup.outerHeight() + popup.outerHeight() + 30 + 'px')
                });/**/

                popup.toggleClass('popup-open');
                if(!popup.hasClass('popup-open')) {
                    popup.removeAttr('style');
                }
            });


            $(close).on('click', function(e) {
                e.preventDefault();
                popup.removeClass('popup-open');

                if(!popup.hasClass('popup-open')) {
                    popup.removeAttr('style');
                }
            });


            $(document).on('click', function(e) {
                if(!popup.is(e.target) && popup.has(e.target).length === 0
                    && !handler.is(e.target) && handler.has(e.target).length === 0) {
                    popup.removeClass('popup-open');

                    if(!popup.hasClass('popup-open')) {
                        popup.removeAttr('style');
                    }
                }
            });


            function getPos() {
                return $(handler).position().top;
            }
        });
    }




    /*
     * Yoga map
     **/
    /*
    function initMap(points) {


        myMap = new ymaps.Map('map', {
            center: [55.755814, 37.617635],
            zoom: 10
        });


        $('#map-filter button[type="submit"]').on('click', function() {
            if (myMap) {
                myMap.destroy();// Деструктор карты
                myMap = null;
            }
        });


        // Создаем коллекцию геообъектов.
        myCollection = new ymaps.GeoObjectCollection();
        myGeoObjects = [];

        $.each(points, function(key, val) {
            // Добавляем в группу метки
            myCollection.add(new ymaps.Placemark([val.x, val.y]));
            // Добавляем коллекцию на карту.
            myMap.geoObjects.add(myCollection);
            // Устанавливаем карте центр и масштаб так, чтобы охватить коллекцию целиком.
            //myMap.setBounds(myCollection.getBounds());
            myMap.setBounds(myCollection.getBounds(), { checkZoomRange: true });


            myGeoObjects[key] = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [val.x, val.y]
                }
            });

            myClusterer = new ymaps.Clusterer();
            myClusterer.add(myGeoObjects);
            myMap.geoObjects.add(myClusterer);
        });
    }/***/



    function yogaCityMap() {

        var map = $('#yoga-map-city');
        var form = $("#map-filter");
        var pagination = $('#map-pagination');
        var inp_page = $('#map-filter-page');
        var submit = function() {
            console.log('submit')

            var data = form.serialize(),
                url,method;

            /* Pin list load */
            url = map.data('ajax-url');
            method = map.data('ajax-method');
            $.ajax({url: url, method: method, data: data, dataType: 'json', success: get_points});

            /* Load yoga cards */
            url = form.attr('action');
            method = form.attr('type');
            $.ajax({url: url, method: method, data: data, dataType: 'json', success: function (resp) {
                var obj = JSON.parse(resp);
                if(obj) {
                    $("#map-list").find('.map-list__item').remove();
                    $.each(obj, function (key, val) {
                        var html = "" +
                            "<div class='map-list__item col-lg-4 col-md-6'>" +
                            "<div class='map-list__card'>" +
                            "<img class='preview' src='" + val.picture + "' alt=''>" +
                            "<a class='title' href='"+ val.url +"'>" + val.title + "</a>" +
                            "<span class='street'>" + val.address + "</span>" +
                            "<span class='caption'>" + val.styles + "</span>" +
                            "<span class='price'>" + val.price + "<span class='rub'></span>" + "</span>" +
                            "<span class='price__exp'>" + val.price_exp + "</span>" +
                            "</div>" +
                            "</div>";
                        $("#map-list").append(html);
                    });
                }
            }});

            /* Load pagination */
            url = pagination.data('ajax-url');
            method = pagination.data('ajax-method');
            $.ajax({url: url, method: method, data: data, dataType: 'html', success: function (resp) {
                console.log(resp)
                pagination.html(resp)
            }});
        };
        var pinmover = function (coord) {
            console.log(coord)
            if (!!userpin){
                userpin.geometry.setCoordinates(coord);
                myMap.setBounds(myMap.geoObjects.getBounds(), { checkZoomRange: true });
            }else{
                userpin = new ymaps.GeoObject({geometry: {type: "Point", coordinates: coord}}, {preset: 'islands#redDotIcon', draggable: true});
                myMap.geoObjects.add(userpin);

                //Drag end listener
                userpin.events.add("dragend", function (event) {
                    $('#map-address-coord').val(userpin.geometry.getCoordinates().join(','))
                    ymaps.geocode(userpin.geometry.getCoordinates(), {kind: 'house'}).then(function (resp) {
                        var obj = !!resp.geoObjects.get(0);

                        //try to get house
                        if(obj){
                            $('#map-address').val(resp.geoObjects.get(0).properties.get('name'));
                        }else{

                            //try to get something
                            ymaps.geocode(userpin.geometry.getCoordinates()).then(function (resp) {
                                $('#map-address').val(resp.geoObjects.get(0).properties.get('name'));
                            })
                        }
                    });
                });
            }
        }

        function initMap() {
            myMap = new ymaps.Map("yoga-map-city", {
                center: [55.755814, 37.617635],
                zoom: 10,
                controls: ['zoomControl']
            });

            var url = map.data('ajax-url');
            var method = map.data('ajax-method');
            $.ajax({url: url, method: method, dataType: 'json', success: get_points});
        }

        function get_points(points){
            if(typeof points != 'object'){points = JSON.parse(points);}
            myMap.geoObjects.removeAll();

            var url = map.data('ajax-card-url');
            var method = map.data('ajax-card-method');


            !!userpin&&myMap.geoObjects.add(userpin)
            myClusterer = new ymaps.Clusterer();
            myGeoObjects = [];

            $.each(points, function(key, val) {
                var geoobject;
                var baloon_loaded = false;
                myGeoObjects[key] = geoobject = new ymaps.GeoObject({
                    geometry: {
                        type: "Point",
                        coordinates: [val.latitude, val.longitude]
                    },
                    properties: {
                        balloonContentHeader: '',
                        balloonContentBody: ''
                    }
                });
                geoobject.events.add('click', function (e) {
                    console.log('click', val.id);
                    if(!baloon_loaded){
                        $.ajax({
                            url: url,
                            method: method,
                            data: 'id='+val.id,
                            dataType: 'json',
                            success: function (resp) {
                                var card = JSON.parse(resp)[0];
                                console.log(card)
                                geoobject.properties.set({
                                    balloonContentHeader: resp.title,
                                    balloonContentBody: "" +
                                    "<div class='b-content'>" +
                                    "<img class='b-picture' src='"+ card.picture +"'>" +
                                    "<span class='b-address'>" + "Адрес:&nbsp;" + card.address + "</span>" +
                                    "<span class='b-style'>" + "Стили:&nbsp;" + card.styles.join(' ,') + "</span>" +
                                    "<span class='b-price'>" + "Стоимость занятия&nbsp;" + card.price + "&nbsp;руб." + "</span>" +
                                    "<a href='"+ card.url +"' class='b-link'>Подробнее о йога-центре</a>"+
                                    "</div>"
                                });
                                baloon_loaded = true;
                                geoobject.balloon.open();
                            }
                        })
                    }
                });

            });

            myClusterer.add(myGeoObjects);
            myMap.geoObjects.add(myClusterer);
            myMap.setBounds(myMap.geoObjects.getBounds(), { checkZoomRange: true });
        }

        function mapForm() {

            form.submit(function(e) {
                e.preventDefault();
                submit();

            });


            $('#map-address').data({stack: false, activity: false});

            //reset page № to 1
            form.on('change', 'select[name], input[name]', function () {
                inp_page.val('1')
            });

            //manipulation with map search
            form.on('input change', '#map-address', function(e, p){
                var _this = $(this);
                if(_this.data('activity') && !p){
                    !_this.data('stack')&&_this.data('stack', true)
                }else{
                    var val = _this.val();
                    _this.data('activity', true);
                    ymaps.geocode($('#map-city').siblings('.ms-parent').find('.ms-choice span').text() + _this.val()).then(
                        function (res) {
                            _this.data('activity', false);
                            var coord = res.geoObjects.get(0).geometry.getCoordinates()
                            $('#map-address-coord').val(coord.join(','))
                            pinmover(coord)
                            if (_this.data('stack')) {
                                _this.data('stack', false);
                                _this.trigger('change',[true]);
                            };
                        },
                        function (err) {
                            _this.data('activity', false);
                            if (_this.data('stack')) {
                                _this.data('stack', false);
                                _this.trigger('change',[true]);
                            };
                        }
                    );
                }
            });

            //change page
            pagination.on('click', 'li', function (e) {
                e.preventDefault();
                if($(this).hasClass('active'))return false;

                var val = inp_page.val()*1;
                var page = $(this).data('page');
                if(page == 'next'){
                    val++;
                    pagination.find('li[data-page="'+val+'"]').addClass('active').siblings().removeClass('active');
                }else if(page == 'prev'){
                    if(val==1){return false;}
                    val--;
                    pagination.find('li[data-page="'+val+'"]').addClass('active').siblings().removeClass('active');
                }else{
                    $(this).addClass('active').siblings().removeClass('active');
                    val = page;
                }
                inp_page.val(val);

                submit();
            });

        }

        $('#define_my_coord').on('click', function (e) {
            e.preventDefault();
            navigator.geolocation.getCurrentPosition(function(position){
                pinmover([position.coords.latitude, position.coords.longitude]);
                setTimeout(function () {
                    userpin.events.fire('dragend')
                },300)
            })
        });

        $.getScript("http://api-maps.yandex.ru/2.1/?lang=ru_RU", function () {
            mapForm();
            ymaps.load(function () {
                initMap();
            });
        });
    }
    if($('#yoga-map-city').length) {yogaCityMap();}


    /*
     * Checkout
     * пошаговое оформление заказа
     * */
    function checkoutInit() {
        var checkout = $('.checkout'),
            back = checkout.find('.order-actions__back'),
            handler = checkout.find('.btn[data-id]');


        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };


        /* Выбор города и способ доставки */
        $('#city').on('change', function() {
            if($(this).find('option:selected').val() == 'Калининград') {
                $('#delivery-kd').removeClass('hidden');
                $('#delivery-more').addClass('hidden');

                if ($('#delivery-kd:not(".hidden")')) {
                    $('#delivery-more').find('input[type=radio]').prop('checked', null);
                    $($('#delivery-kd:not(".hidden")')).find('.form-check:first input[type=radio]').trigger('click');
                    $('#delivery-address').removeClass('hidden');
                }
            } else {
                $('#delivery-kd').addClass('hidden');
                $('#delivery-more').removeClass('hidden');

                if ($('#delivery-more:not(".hidden")')) {
                    $('#delivery-kd').find('input[type=radio]').prop('checked', null);
                    $($('#delivery-more:not(".hidden")')).find('.form-check:first input[type=radio]').trigger('click');
                    $('#delivery-address').addClass('hidden');
                }
            }
        });


        /* Выбор способа доставки */
        $('#delivery-choice').on('change', function() {
            if(~$(this).find('input[type=radio]:checked').siblings().text().indexOf('Самовывоз со склада')) {
                $('#delivery-warehouse').removeClass('hidden');
                $('#courier-info').addClass('hidden');
                $('#delivery-address').addClass('hidden');
                $('#delivery-post').addClass('hidden');
                $('#delivery-free').addClass('hidden');
            } else if(~$(this).find('input[type=radio]:checked').siblings().text().indexOf('Курьерская доставка Грастин')) {
                $('#courier-info').removeClass('hidden');
                $('#delivery-address').removeClass('hidden');
                $('#delivery-post').addClass('hidden');
                $('#delivery-warehouse').addClass('hidden');
                $('#delivery-free').addClass('hidden');
            } else if(~$(this).find('input[type=radio]:checked').siblings().text().indexOf('Курьерская служба СДЭК')) {
                $('#courier-info').removeClass('hidden');
                $('#delivery-address').addClass('hidden');
                $('#delivery-post').removeClass('hidden');
                $('#delivery-warehouse').addClass('hidden');
                $('#delivery-free').addClass('hidden');
            } else if(~$(this).find('input[type=radio]:checked').siblings().text().indexOf('Экспедиционная компания ПЭК')) {
                    $('#courier-info').removeClass('hidden');
                    $('#delivery-address').addClass('hidden');
                    $('#delivery-post').removeClass('hidden');
                    $('#delivery-warehouse').addClass('hidden');
                    $('#delivery-free').addClass('hidden');
            } else if(~$(this).find('input[type=radio]:checked').siblings().text().indexOf('Самовывоз')) {
                $('#delivery-free').removeClass('hidden');
                $('#courier-info').addClass('hidden');
                $('#delivery-address').addClass('hidden');
                $('#delivery-post').addClass('hidden');
                $('#delivery-warehouse').addClass('hidden');
            } else {
                $('#courier-info').addClass('hidden');
                $('#delivery-address').addClass('hidden');
                $('#delivery-post').addClass('hidden');
                $('#delivery-warehouse').addClass('hidden');
                $('#delivery-free').addClass('hidden');
            }
        });


        /* Самовывоз
         * Выбор пункта */
        $('#delivery-free .point-list').on('click', '.point-list__item', function() {
            var item = $(this);
            item.addClass('active').siblings().removeClass('active').find('.point-content__item').removeClass('active');
        });


        /* Выбор доставки до двери */
        $('#delivery-post').on('change', function() {
            if($(this).find('input[type=checkbox]').is(':checked')) {
                $('#delivery-address').removeClass('hidden');
            } else {
                $('#delivery-address').addClass('hidden');
            }
        });




        function getListDelivery() {
            console.log('qq');
            $.getJSON('delivery.json', function (data) {
                var delivery = [];
                var list = $('.point-list');


                //load metro
                $.getJSON('list_of_moscow_metro_stations.json', function (data) {
                    console.log('metro load', data);

                    $.map(data, function (e) {
                        $('#stations').append("<option value='" + e['Latitude'] + ',' + e['Longitude'] + "'>" + e['Name'] + "</option>");
                    });

                    $('#stations').multipleSelect({
                        placeholder: $(this).attr('data-placeholder'),
                        width: '100%',
                        maxHeight: 'auto',
                        single: true,
                        selectAll: false,
                        selectAllText: 'Выбрать всё',
                        selectAllDelimiter: '',
                        allSelected: '',
                        countSelected: 'Выбранно # из %'
                    });
                    $('#stations')
                        .siblings('.ms-parent')
                        .find('.ms-drop')
                        .mCustomScrollbar({
                            scrollbarPosition: 'outside',
                            scrollInertia: 0
                        });
                });



                //load html
                $.each(data, function (key, val) {
                    var coord = [val.x, val.y];

                    // Получаем список пунктов доставки
                    var html = '' +
                        '<li class="point-list__item" id="' + key + 'i">' +
                        '<div class="point-list__content">' +
                        '<p><strong data-role="distanse"></strong></p>' +
                        '<p><strong>Адрес</strong>:' + val.address + '</p>' +
                        '</div>' +
                        '<div class="point-tabs">' +
                        '<a href="#checkout">Как до нас добраться?</a>' +
                        '<a href="#schema">Схема проезда</a>' +
                        '<a href="#ya-map">Яндекс карта</a>' +
                        '</div>' +
                        '<div class="point-content">' +
                        '<div data-id="checkout" class="point-content__item">' + val.checkout + '</div>' +
                        '<div data-id="schema" class="point-content__item"><img src="' + val.schema + '" alt=""></div>' +
                        '<div data-id="ya-map" class="point-content__item"><div id="map-'+ key +'i" data-map-coord="' +coord+ '" class="point-content__map"></div></div>' +
                        '</div>' +
                        '</li>';

                    val.id = key+'i';
                    delivery.push(val);
                    list.append(html);

                    $('#'+key+'i').data('coord', coord);

                    //yaMaps(key, coord);
                });

                console.log(delivery);


                var isFlag = true;
                $('#stations').on('change', function (e) {
                    //e.stopPropagation();
                    e.preventDefault();
                    var elm = $(this);

                    if(isFlag) {
                        isFlag = false;

                        console.log(elm.val());
                        change_range(elm.val(), function () {
                            delivery = delivery.sort(compare);
                            console.log(delivery);
                            view_update(elm.find('option:selected').text());

                            isFlag = true;
                        });
                    }
                });



                $('.point-list__item').on('click', '.point-tabs > a', function (e) {
                    e.preventDefault();
                    var link = $(this),
                        linkAttr = link.attr('href').replace('#', ''),
                        content = link.parents().siblings('.point-content'),
                        item = content.find('div[data-id=' + linkAttr + ']');
                    //console.log(linkAttr);
                    $(link).toggleClass('active').siblings().removeClass('active');
                    $(item).toggleClass('active').siblings().removeClass('active');
                });


                $('.point-list__item').on('click', '.point-tabs > a[href="#ya-map"]', function () {
                    var parent = $(this).parents('.point-list__item');
                    var itemId = parent.attr('id');
                    var key = itemId;
                    var map = $('#map-'+itemId);
                    var mapCoord = map.data('map-coord');
                    //console.log(key, mapCoord);
                    yaMaps(key, mapCoord);
                });


                function view_update(name) {
                    console.log(name);
                    $.map(delivery, function (e) {
                        var elm = $('#'+e.id);
                        elm.find('[data-role="distanse"]').text(e.range + 'км. до '+ name);
                        list.append(elm.detach());
                    })
                }

                function compare(a,b) {
                    if (a.range < b.range)
                        return -1;
                    if (a.range > b.range)
                        return 1;
                    return 0;
                }


                function change_range(m, callback) {

                    $.map(delivery, function (e,i) {
                        var coord = [e.x, e.y];

                        ymaps.route([
                            coord,
                            m
                        ]).then(function (route) {


                            //myMap.geoObjects.add(route);
                            // Зададим содержание иконок начальной и конечной точкам маршрута.
                            // С помощью метода getWayPoints() получаем массив точек маршрута.
                            // Массив транзитных точек маршрута можно получить с помощью метода getViaPoints.
                            var points = route.getWayPoints(),
                                lastPoint = points.getLength() - 1;
                            // Задаем стиль метки - иконки будут красного цвета, и
                            // их изображения будут растягиваться под контент.
                            points.options.set('preset', 'islands#redStretchyIcon');
                            // Задаем контент меток в начальной и конечной точках.
                            points.get(0).properties.set('iconContent', 'Точка отправления');
                            points.get(lastPoint).properties.set('iconContent', 'Точка прибытия');


                            e.range = Math.round(route.getLength() / 1000);



                            if(i==delivery.length-1){
                                callback();
                            }


                        }, function (error) {
                            alert('Возникла ошибка: ' + error.message);
                        });


                    });

                }

            });
        }



        function yaMaps(key, mapCoord) {
            var myMap, myPlacemark;
            var coord = mapCoord.split(',');

            myMap = new ymaps.Map("map-" + key + "", {
                center: coord,
                zoom: 16
            });


            $('.point-list__item .point-tabs > a[href="#ya-map"]').bind({
                click: function () {
                    if (myMap) {
                        myMap.destroy();// Деструктор карты
                        myMap = null;
                    }
                }
            });

            //console.log(mapCoord);
            //console.log(coord);
            //console.log(myMap.getCenter());
            myPlacemark = new ymaps.Placemark(coord);
            myMap.geoObjects.add(myPlacemark);
        }



        function initme() {
            console.log('ymap loaded');
            ymaps.load(getListDelivery);
        }
        /**/
        $.getScript("//api-maps.yandex.ru/2.1/?lang=ru_RU", initme);


        // back step
        $(back).on("click", function(e) {
            e.preventDefault();
            var href = $(this).attr('href');
            $('html, body').animate({
                scrollTop: $(href).offset().top
            }, 800);
        });

        // next step
        $(handler).on("click", function(e) {
            e.preventDefault();
            var dataId = $(this).data('id');
            //console.log(dataId);

            $('#'+dataId).addClass('visible');

            $('html, body').animate({
                scrollTop: $('#'+dataId).offset().top
            }, 800);
        });



        /*
         $('.checkout-form').validate({
         rules: {
         name: {
         required: true,
         minlength: 2
         }
         },
         messages: {
         name: {
         required: "Поле 'Имя' обязательно к заполнению",
         minlength: "Введите не менее 2-х символов в поле 'Имя'"
         },
         email: {
         required: "Поле 'Email' обязательно к заполнению",
         email: "Необходим формат адреса email"
         },
         url: "Поле 'Сайт' обязательно к заполнению"
         }
         });/**/
    }


    function accordionInit() {
        $('[data-accordion]').each(function(i) {
            var accordion = $(this).eq(i),
                toggle = accordion.find('[data-accordion-toggle]');

            $(toggle).on('click', function(e) {
                e.preventDefault();
                var $this = $(this).parents('[data-accordion-item]'),
                    header =  $this.find('[data-accordion-header]'),
                    toggle = $this.find('[data-accordion-toggle]'),
                    content = $this.find('[data-accordion-content]');

                $this
                    .find('[data-accordion-content]')
                    .slideDown(350)
                    .parents('[data-accordion-item]')
                    .addClass('show')
                    .siblings()
                    .removeClass('show')
                    .find('[data-accordion-content]')
                    .slideUp(350);
            });
        });
    }



    function optCart() {
        $('#opt').each(function() {
            var opt = $(this);
            var products = opt.find('#opt-products');
            var addProduct = opt.find('#add-product');

            var menu = $('#opt-menu');
            var link = menu.find('a[data-toggle="link"]');
            var currentPosition = 0;
            var heading = [];
            var header = $('.page-title');

            $(addProduct).on('click', function(e) {
                e.preventDefault();
                var nextTitle = $(this).attr("data-title");
                products.css('left', -$(products).outerWidth());
                menu.css('left', 0);
                opt.css('height', menu.height());
                $('.sidebar').hide();
                $('.page-title').html(nextTitle);
            });

            if($('[disabled]').length) {
                $('[disabled]').find('[data-opt-menu] input').prop('disabled', true);
            }

            $(link).on('click', function(e) {
                e.preventDefault();
                var $this = $(this);
                var name = $(this).find('[data-name]').text();
                var item = $(this).parent();
                var currentItem = $(this).siblings('ul');

                if($this.parent('[disabled]').length) { return false; }

                var resizeWindow = function(currentPosition) {
                    if($(window).width() < 768) {
                        $(menu).css({left: -(currentItem.outerWidth() * currentPosition)});
                        $(opt).css({height: currentItem.height()});
                        $(currentItem).css('visibility', 'visible');
                        heading.push(header.text());
                        header.html(name);
                    } else {
                        if($this.parents('[data-item="size"]').length) {return true;}

                        item.toggleClass('open');
                        if(item.hasClass('open')) {
                            currentItem.show();
                        } else {
                            item.find('.open').removeClass('open').children('[data-opt-menu]').hide();
                            currentItem.hide();
                        }
                    }
                };

                if(currentItem.length) {
                    currentPosition = currentPosition + 1;
                    resizeWindow(currentPosition);

                    $(window).resize(resizeWindow(currentPosition));

                } else {
                    return false;
                }
            });


            $('a[data-toggle="back"]').on('click', function(e) {
                e.preventDefault();
                var container = $(this).parents('[data-opt-menu]').eq(1);
                currentPosition = currentPosition - 1;


                var resizeWindow = function(currentPosition) {
                    if(currentPosition == -1) {
                        currentPosition = 0;
                        products.css('left', 0);
                        $(opt).css({height: products.height()});
                        $(menu).css({left: (menu.outerWidth())});
                        $('.sidebar').show();

                        if($('a[data-toggle="back"][data-title]').length) {
                            $('.page-title').html($('a[data-toggle="back"]').attr('data-title'));
                        }

                    } else {
                        $(menu).css({left: -(menu.outerWidth() * currentPosition)});
                        $(opt).css({height: container.height()});
                        header.html(heading.pop());
                    }
                };

                resizeWindow(currentPosition);
                $(window).resize(resizeWindow(currentPosition));
            });



            $('[data-id="add-to-card"]').on('click', function(e) {
                e.preventDefault();
                currentPosition = 0;

                $('#opt-final').show();
                opt.css('height', $('#opt-final').height());
                menu.css('display', 'none');
                header.css('display', 'none');
                $('.search').css('display', 'none');
            });


            $('[data-id="category"]').on('click', function(e) {
                e.preventDefault();
                currentPosition = 0;

                var title = $(this).attr('data-title');

                $('#opt-final').hide();
                menu.css({
                    'display': 'block',
                    'left': 0
                });
                opt.css('height', menu.height());
                header.html(title);
                header.css('display', 'block');
                $('.search').css('display', 'block');
            });


            $('[data-id="form"]').on('click', function(e) {
                e.preventDefault();
                var title = $(this).attr('data-title');

                $('#opt-final').hide();
                menu.css({
                    'display': 'block',
                    'left': menu.outerWidth()
                });
                products.css('left', 0);
                $('.sidebar').css('display', 'block');
                opt.css('height', products.height());
                header.html(title);
                header.css('display', 'block');
                $('.search').css('display', 'block');
            });


        });
    }



    /*** --- DOM ready --- ***/
    $(function() {
        var isMobile = {
            Android: function() {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function() {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function() {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function() {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function() {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function() {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };


        $('select:not("#stations")').each(function() {
            var single = !$(this).is('[multiple="multiple"]');
            $(this).multipleSelect({
                placeholder: $(this).attr('data-placeholder'),
                width: '100%',
                maxHeight: 'auto',
                single: single,
                selectAll: false,
                selectAllText: 'Выбрать всё',
                selectAllDelimiter: '',
                allSelected: '',
                countSelected: 'Выбранно # из %'
            });
            //if (!single && $(this).multipleSelect('getSelects').length == 0){$(this).multipleSelect('checkAll')};
            $(this).siblings('.ms-parent').find('.ms-drop').mCustomScrollbar({
                scrollbarPosition: 'outside',
                scrollInertia: 0
            });
        });


        /*
         * Поддержка SVG в IE
         **/
        svg4everybody();

        if($('[data-mask]').length) {
            $("[data-mask]").mask("+7 999 999 99 99");
        }

        if($(".js-nav").length) {
            navigation();
        }


        if($('[data-accordion]').length) {
            accordionInit();
        }


        /*
         * Инициализация слайдера товара
         **/
        if($("#product-slider").length) {
            InitializeSlider();
        }

        /*
         * Вызываем функцию Quantity
         **/
        if($("[data-id='quantity']").length) {
            quantity();
        }

        if($(".js-filter-trigger").length) {
            $(".js-filter-trigger").on("click", function (e) {
                e.preventDefault();

                $(this).toggleClass("open");
                $(this).parent().next().slideToggle();
            });

            $(window).on("resize", function() {
                if($(window).width() > 767) {
                    $(".sidebar-overlay").removeAttr("style");
                }
            });
        }

        /*
         * Инициализация datepicker
         **/
        if($('.js-date').length) {
            $('.js-date').datepicker({
                language: 'ru',
                format: 'dd MM yy'
            });
        }

        if($('[data-accordion]').length) {
            initializeAccordions();
        }

        if($("[data-zoom]").length) {
            zoomInitialize();
        }


        if($('.table-accordion').length) {
            tableAccordion();
        }


        if($('[data-popup]').length) {
            popupInit();
        }

        if($('.checkout').length) {
            checkoutInit();
        }

        if($('form').length) {
            //formValidation();

            $.fn.formValidates = function () {
                return this.each(function () {

                    /* Объявляем кнопки */
                    var $form = $(this),
                        $input = $form.find('input'),
                        $sInput = $form.find('input.subscribe__button'),
                        $submit = $form.find('button[type="submit"]'),
                        dNameForm = $form.attr('data-validate');


                    var validate = {
                        /* Объявляем константы */
                        init: {
                            $this: this,
                            minlenPhone: '6',
                            lenPhone: '11',
                            minLenName: '2',
                            minLenNum: '1',
                            minLenText: '1',
                            minLenPassword: '5',
                            regExPhone: /[^0-9]/, // регэкс разрешенных цифр
                            regExEmail: /^[\w\.\d-_]+@[\w\.\d-_]+\.\w{2,8}$/i, // регэкс проверяем email по маске
                            regExText: /[^A-Za-zА-Яа-яёЁ_]$/, // регэкс разрешенных символов
                            regExTextNum: /[^A-Za-zА-Яа-яёЁ\s\d]/, // регэкс разрешенных символов и букв
                            regExNum: /[^0-9]/, // регэкс разрешенных цифр
                            lang: 'ru', //Передаем язык
                            fPesonal: 'personal',
                            fRegister: 'registration'
                        },
                        action: function () {
                            /*Как будем проверять*/
                            var $this = this;

                            $submit.on('click', function (event) {
                                var arrError = []; //Список полей с ошибками
                                $input.each(function (i, element) {
                                    ($this.eachInput(element)) ? arrError.push($this.eachInput(element)) : false;
                                });
                                if (arrError.length > '1')
                                    event.preventDefault();
                            });

                            /*Специально для подписки*/
                            $sInput.on('click', function (event) {
                                var arrError = []; //Список полей с ошибками
                                $input.each(function (i, element) {
                                    ($this.eachInput(element)) ? arrError.push($this.eachInput(element)) : false;
                                });
                                (arrError.length >= '1') ? event.preventDefault() : ajax_email($input.val());
                            });

                            /*Специально для подписки*/
                            $("#subscribe .btn[type=submit]").on('click', function (event) {

                                var arrError = []; //Список полей с ошибками
                                $input.each(function (i, element) {
                                    ($this.eachInput(element)) ? arrError.push($this.eachInput(element)) : false;
                                });
                                (arrError.length >= '1') ? event.preventDefault() : ajax_subscribe_modal($('#subscribe').find("input[name=name]").val(), $('#subscribe').find("input[name=email]").val());
                            });


                            $input.each(function (i, element) {
                                //console.log(i, element);

                                if ($(element).val().length != 0) {
                                    $(element).each(function () {
                                        $this.eachInput(element);
                                    });
                                }

                                $(element).on('change, keyup', function () {
                                    $this.eachInput(element);
                                });
                            });
                        },
                        eachInput: function (element) {
                            var eName = $(element).attr('data-name'),
                                eVal = $(element).val(), error;

                            switch (eName) {
                                case 'number':
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.minLenNum > eVal.length) {
                                        this.print(element, 'lenNum');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.regExNum.test(eVal)) {
                                        this.print(element, 'errorNum');
                                        error = this.addErrorClass(element);

                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'text':
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.minLenText > eVal.length) {
                                        this.print(element, 'lenText');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.regExText.test(eVal)) {
                                        this.print(element, 'errorText');
                                        error = this.addErrorClass(element);

                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;
                                case 'text_and_number':
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.minLenText > eVal.length) {
                                        this.print(element, 'lenTextNum');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.regExTextNum.test(eVal)) {
                                        this.print(element, 'errorTextNum');
                                        error = this.addErrorClass(element);

                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'login':
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);
                                    } else if (this.init.minLenName > eVal.length) {
                                        this.print(element, 'lenName');
                                        error = this.addErrorClass(element);
                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'name':

                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);
                                    } else if (this.init.minLenName > eVal.length) {
                                        this.print(element, 'lenName');
                                        error = this.addErrorClass(element);
                                    } else if (this.init.regExText.test(eVal)) {
                                        this.print(element, 'errorName');
                                        error = this.addErrorClass(element);
                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'phone':
                                    //eVal = eVal.replace(this.init.regExPhone, "");
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);
                                    } else if (this.init.minlenPhone > eVal.length) {
                                        this.print(element, 'lenPhone');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.regExPhone.test(eVal)) {
                                        this.print(element, 'errorPhone');
                                        error = this.addErrorClass(element);

                                    } else if (this.init.lenPhone == eVal.length) {
                                        /*Дополнительная проверка на валидацию в личном кабинете*/
                                        var url = window.location.pathname;
                                        if (~url.indexOf('auth')) {
                                            var UV = user_valid(eVal);
                                            if (UV == 'Y' && dNameForm == this.init.fPesonal || UV == 'Y' && dNameForm == this.init.fRegister) {
                                                this.print(element, 'yesPhone');
                                                error = this.addErrorClass(element);
                                            } else if (UV != 'Y' && dNameForm != this.init.fPesonal && dNameForm != this.init.fRegister) {
                                                this.print(element, 'noPhone');
                                                error = this.addErrorClass(element);
                                            } else {
                                                this.addValidClass(element);
                                            }
                                        } else {
                                            this.addValidClass(element);
                                        }

                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'email':
                                    if (eVal.length < 1) {
                                        this.print(element, 'required');
                                        error = this.addErrorClass(element);
                                    } else if (!this.init.regExEmail.test(eVal)) {
                                        this.print(element, 'regExEmail');
                                        error = this.addErrorClass(element);
                                    } else {
                                        this.addValidClass(element);
                                    }
                                    break;

                                case 'password':

                                    /**
                                     * @todo переделать валидацию на что-то более адекватное
                                     */
                                    /*
                                     if (eVal.length < 1) {
                                     this.print(element, 'required');
                                     error = this.addErrorClass(element);
                                     } else if (this.init.minLenPassword >= eVal.length) {
                                     this.print(element, 'lenPasswords');
                                     error = this.addErrorClass(element);
                                     } else {
                                     this.addValidClass(element);
                                     }/* */

                                    this.addValidClass(element);
                                    break;

                                default:
                                    break;
                            }
                            return error;
                        },
                        addValidClass: function (element) {
                            $(element).next().html("");
                            $(element).removeClass('error');
                            $(element).parent().removeClass('error');
                            $(element).addClass('valid');
                            $(element).parent().addClass('valid');
                        },
                        addErrorClass: function (element) {
                            $(element).removeClass('valid');
                            $(element).parent().removeClass('valid');
                            $(element).addClass('error');
                            $(element).parent().addClass('error');
                            return $(element).attr('data-name');
                        },
                        print: function (el, text) {
                            $(el).next().html(this.message[this.init.lang][text]);
                        },
                        /* Сообщения */
                        message: {
                            ru: {
                                /*Text*/
                                lenText: "Слишком мало символов",
                                errorText: 'Ошибка при вводе, недопустимые символы',
                                /*Text & num*/
                                lenTextNum: "Слишком мало символов",
                                errorTextNum: 'Ошибка при вводе, недопустимые символы',
                                /*Name*/
                                lenName: "Слишком мало символов",
                                errorName: 'Ошибка при вводе, недопустимые символы',
                                /*Phone*/
                                lenPhone: "Не верный телефонный номер",
                                noPhone: "Такой телефон отсутствует в базе",
                                yesPhone: "Такой телефон уже зарегистрирован",
                                /*Password*/
                                lenPasswords: "Слишком мало символов",
                                /*Email*/
                                regExEmail: "Введите верный Email",
                                /*any*/
                                required: "Вы не заполнили обязательное поле"
                            },
                            en: {
                            }
                        }
                    };

                    validate.action();
                });
            };



            $('[data-validate="checkout"]').formValidates();
            $('[data-validate="add-yoga"]').formValidates();
            $('[data-validate="faq-question"]').formValidates();

        }


        $('.form-add-yoga').each(function() {
            var $form = $(this);
            var checkbox = $form.find('input[type="checkbox"]');
            var btn = $form.find('button');
            console.log(checkbox);

            checkbox.on('change', function() {
                if(checkbox.is(':checked') == true) {
                    btn.prop('disabled', false);
                } else {
                    btn.prop('disabled', true);
                }
            });

        });


        if($('#opt').length) {
            optCart();
        }

    }());
}(jQuery));
