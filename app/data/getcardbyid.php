<?
 $json = '[{
    "url": "/url",
    "picture": "../images/map/item-1.jpg",
    "title": "«Калининградская Федерация Классической Йоги»",
    "address": "ул. Больничная, 45, зал № 29А",
    "price": "250",
    "price_exp": "за занятие",
    "styles": ["Хатха-йога", "Йога для беременных", "Йога для детей", "Парная йога (йога с партнером)", "Медитация"],
    "latitude": 54.711000,
    "longitude": 20.503678
}]';


    print json_encode($json, JSON_UNESCAPED_UNICODE)

?>
