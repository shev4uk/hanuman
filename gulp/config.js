'use strict';

var path = require('path');
var modRewrite = require('connect-modrewrite');


// Default settings
module.exports.uglifyJs = process.env.UGLIFYJS || false; // to remove .min sufix edit template manually
module.exports.minifyCss = process.env.MINIFYCSS || false; // to remove .min sufix edit template manually
module.exports.cacheBust = process.env.CACHEBUST || false;


// Default paths
var tmp = ".tmp";
var app = "app";
var dist = "dist";
var bowerDir = 'bower_components';
var nodeDir = 'node_modules';

// Default paths in app folder
var css = "styles";
var js = "scripts";
var images = "images";
var icons = "icons";
var fonts = "fonts";
var views = "views";



// Rewrite rules enables removing .html extensions in development.
// This are possible routes for same test.html file:
// http://localhost:3000/test.html
// http://localhost:3000/test
var rewriteRules = [
    '^/$ - [L]', // default site root handling (index.html)
    '.html$ - [L]', // ignore routes ends with '.html'
    '(.*)/$ $1/index.html [L]', // routes with trailing slash are directories -> rewrite to directory index.html
    '\\/\[a-zA-Z0-9_\\-\@.]+\\.\[a-zA-Z0-9]+$ - [L]', // ignore files with extension (eg. .css, .js, ...)
    '(.*)$ $1.html [L]' // redirect routes ends with string without trailing slash to original html
];



// Browser sync task config
module.exports.browserSync = {
    dev: {
        server: {
            baseDir: [tmp, app],
            routes: {
                '/bower_components': bowerDir,
                '/node_modules': nodeDir
            }
        },
        notify: false,
        debugInfo: false,
        host: 'localhost',
        middleware: [
            modRewrite(rewriteRules)
        ]
    },
    dist: {
        server: {
            baseDir: dist
        },
        notify: false,
        debugInfo: false,
        host: 'localhost',
        middleware: [
            modRewrite(rewriteRules)
        ]
    }
};



// Build size task config
module.exports.buildSize = {
    srcAll: dist + '/**/*',
    cfgAll: {
        title: 'build',
        gzip: true
    },
    srcCss: path.join(dist, css, '/**/*'),
    cfgCss: {
        title: 'CSS',
        gzip: true
    },
    srcJs: path.join(dist, css, '/**/*'),
    cfgJs: {
        title: 'JS',
        gzip: true
    },
    srcImages: path.join(dist, images, '/**/*'),
    cfgImages: {
        title: 'Images',
        gzip: false
    }
};


module.exports.clean = [tmp, dist];


module.exports.copyFonts = {
    src: [
        path.join(app, fonts, "**/*")
    ],
    dest: dist + '/fonts'
};


module.exports.copyExtras = {
    src: [
        app + '/*.*',
        '!' + app + '/*.html'
    ],
    dest: dist,
    cfg: {
        dot: true
    }
};


module.exports.images = {
    src: path.join(app, images, "**/*"),
    dest: dist + '/images'
};

module.exports.sprite = {
    svg: {
        src: path.join(app, icons, "**/*.svg"),
        dest: tmp + "/icons",
        cfg: {
            mode: {
                symbol: {
                    render: {
                        css: false, // CSS output option for icon sizing
                        scss: false // SCSS output option for icon sizing
                    },
                    dest: '.',
                    sprite: 'icons',
                    inline: false,
                    example: true // Build a sample page, please!
                }
            }
        }
    },
    png: {
        src: path.join(app, icons, "**/*.png"),
        dest: tmp + "/icons",
        cfg: {
            imgName: 'sprite.png',
            cssName: 'sprite.css'
        }
    }
};


module.exports.scripts = {
    src: path.join(app, js, "*.js"),
    dest: path.join(tmp, js)
};


module.exports.styles = {
    src: path.join(app, css, "styles.scss"),
    dest: path.join(tmp, css),
    sassCfg: {style: 'expanded'},
    autoprefixerCfg: {browsers: ['last 2 version']}
};


module.exports.templates = {
    src: path.join(app, views, '*.pug'),
    srcBuild: path.join(tmp, 'jade/*.pug'),
    dest: tmp,
    destBuild: path.join(dist),
    cfg: {
        pretty: true
    }
};


module.exports.useref = {
    src: path.join(app, views, '/**/*.pug'),
    dest: dist,
    destJade: path.join(tmp, 'jade'),
    assetsCfg: {
        searchPath: app
    },
    revManifestCfg: {merge: true}
};



module.exports.watch = {
    styles: path.join(app, css, '/**/*.scss'),
    scripts: path.join(app, js, '/**/*.js'),
    svg: path.join(app, icons, '/**/*.svg'),
    tmp: path.join(app, views, '/**/*.pug'),
    wiredep: 'bower.json'
};


// Wiredep task config
module.exports.wiredep = {
    sass: {
        src: path.join(app, css, '/*.scss'),
        dest: path.join(app, css),
        cfg: {
            ignorePath: '../../',
            overides: {}
        }
    },
    jade: {
        src: path.join(app, views, '/includes/*.pug'),
        dest: path.join(app, views, '/includes'),
        cfg: {
            //exclude: [''],
            ignorePath: '../../',
            overides: {}
        }
    }
};
