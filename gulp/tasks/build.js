
var gulp = require('gulp-help')(require('gulp'));
var watch = require("gulp-watch");
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var useref = require("gulp-useref");
var plumber = require('gulp-plumber');
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var minifyCss = require('gulp-clean-css');
var prefixer = require("gulp-autoprefixer");
var sprite = require("gulp-svg-sprite");
var notifier = require("node-notifier");
var concat = require("gulp-concat");
var del = require('del');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
//var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var size = require('gulp-size');

var config = require('./../config.js');
var build = require('./../utils/buildHelper.js');



// Output size of dist folder
gulp.task('buildSize:css', false, function () {
    return gulp.src(config.buildSize.srcCss)
        .pipe(size(config.buildSize.cfgCss));
});

// Output size of dist folder
gulp.task('buildSize:js', false, function () {
    return gulp.src(config.buildSize.srcJs)
        .pipe(size(config.buildSize.cfgJs));
});

// Output size of dist folder
gulp.task('buildSize:images', false, function () {
    return gulp.src(config.buildSize.srcImages)
        .pipe(size(config.buildSize.cfgImages));
});


// Output size of dist folder
gulp.task('buildSize', 'Determine size of `dist/` folder', ['buildSize:css', 'buildSize:js','buildSize:images'], function () {
    return gulp.src(config.buildSize.srcAll)
        .pipe(size(config.buildSize.cfgAll));
});




gulp.task('build', function(cb) {
    build.setBuild(true);
    runSequence(
        ['wiredep', 'clean'],
        ['styles', 'scripts', 'sprite'],
        ['images', 'copy', 'extras'],
        'templates',
        'buildSize',
        function() {
            notifier.notify({
                title: 'Build',
                message: 'Build was successful'
            });
            cb();
        }
    );
});
