
var gulp = require('gulp-help')(require('gulp'));
var watch = require("gulp-watch");
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var useref = require("gulp-useref");
var plumber = require('gulp-plumber');
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var minifyCss = require('gulp-clean-css');
var prefixer = require("gulp-autoprefixer");
var spritesmith = require("gulp.spritesmith");
var sprite = require("gulp-svg-sprite");
var svgo = require("svgo");
var notify = require("gulp-notify");
var concat = require("gulp-concat");
var del = require('del');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
//var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var size = require('gulp-size');

var config = require('./../config.js');


gulp.task("sprite:svg", function() {
    return gulp.src(config.sprite.svg.src)
        .pipe(sprite(config.sprite.svg.cfg))
        .pipe(gulp.dest(config.sprite.svg.dest))
        .pipe(reload({stream: true}));
});


gulp.task("sprite:png", function() {
    return gulp.src(config.sprite.png.src)
        .pipe(spritesmith(config.sprite.png.cfg))
        .pipe(gulp.dest(config.sprite.png.dest))
        .pipe(reload({stream: true}));
});


gulp.task("sprite", ['sprite:svg', 'sprite:png']);
