
var gulp = require('gulp-help')(require('gulp'));
var watch = require("gulp-watch");
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var rev = require('gulp-rev');
var fs = require('fs');
var revReplace = require('gulp-rev-replace');
var filter = require('gulp-filter');
var useref = require("gulp-useref");
var plumber = require('gulp-plumber');
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var cssnano = require('gulp-cssnano');
var prefixer = require("gulp-autoprefixer");
var sprite = require("gulp-svg-sprite");
var notify = require("gulp-notify");
var concat = require("gulp-concat");
var del = require('del');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var size = require('gulp-size');

var config = require('./../config.js');
var handleError = require('./../utils/handleError.js');
var build = require('./../utils/buildHelper.js');


gulp.task("templates", ['useref'], function() {
    var src = build.isBuild() ? config.templates.srcBuild : config.templates.src;
    var dest = build.isBuild() ? config.templates.destBuild : config.templates.dest;

    return gulp.src(src)
        .pipe(plumber(handleError))
        .pipe(pug(config.templates.cfg))
        .pipe(gulp.dest(dest));
});


gulp.task('useref', function() {
    // run useref only in build
    if (build.isBuild()) {
        var jadeFilesOnly = filter(['**/*.pug'], {restore: true});
        var excludeJade = filter(['**', '!**/*.pug']);

        return gulp.src(config.useref.src)
            .pipe(useref())
            .pipe(gulpif('*.js', gulpif(config.uglifyJs, uglify()))) // uglify JS
            .pipe(gulpif('*.css', gulpif(config.minifyCss, cssnano()))) // minify CSS
            .pipe(gulpif('!**/*.pug', gulpif(config.cacheBust, rev())))
            .pipe(gulpif(config.cacheBust, revReplace({replaceInExtensions: ['.pug', '.css', '.js']})))
            .pipe(jadeFilesOnly)
            .pipe(gulp.dest(config.useref.destJade))
            .pipe(jadeFilesOnly.restore)
            .pipe(excludeJade)
            .pipe(gulp.dest(config.useref.dest))
            .pipe(gulpif(config.cacheBust, rev.manifest(config.useref.revManifestCfg))) // create rev-manifest.json
            .pipe(gulp.dest(config.useref.dest));
    } else {
        return;
    }
});
