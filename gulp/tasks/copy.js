
var gulp = require('gulp-help')(require('gulp'));
var watch = require("gulp-watch");
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var useref = require("gulp-useref");
var plumber = require('gulp-plumber');
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var minifyCss = require('gulp-clean-css');
var prefixer = require("gulp-autoprefixer");
var sprite = require("gulp-svg-sprite");
var notify = require("gulp-notify");
var concat = require("gulp-concat");
var del = require('del');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
//var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var size = require('gulp-size');

var config = require('./../config.js');


// copy font to dist folder
gulp.task('fonts', 'Copy fonts to `dist/`', function () {
    return gulp.src(config.copyFonts.src)
        .pipe(gulp.dest(config.copyFonts.dest));
});

// copy extras in app/ directory
gulp.task('extras', 'Copy extras in `app/` root to `dist/`', function () {
    return gulp.src(config.copyExtras.src, config.copyExtras.cfg)
        .pipe(gulp.dest(config.copyExtras.dest));
});

gulp.task('copy', 'Copy fonts and extras to `dist/` folder', ['fonts', 'extras']);
