
var gulp = require('gulp-help')(require('gulp'));
var watch = require("gulp-watch");
var uglify = require('gulp-uglify');
var gulpif = require('gulp-if');
var useref = require("gulp-useref");
var plumber = require('gulp-plumber');
var pug = require("gulp-pug");
var sass = require("gulp-sass");
var minifyCss = require('gulp-clean-css');
var prefixer = require("gulp-autoprefixer");
var sprite = require("gulp-svg-sprite");
var notify = require("gulp-notify");
var concat = require("gulp-concat");
var del = require('del');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
//var wiredep = require('wiredep').stream;
var runSequence = require('run-sequence');
var size = require('gulp-size');

var config = require('./../config.js');


gulp.task("styles", function() {
    var onError = function(err) {
        notify.onError({
            title:    "Gulp",
            subtitle: "Failure!",
            message:  "Error: <%= error.message %>",
            sound:    "Beep"
        })(err);

        this.emit('end');
    };


    return gulp.src(config.styles.src)
        .pipe(plumber({errorHandler: onError}))
        .pipe(sass(config.styles.sassCfg))
        .pipe(prefixer(config.styles.autoprefixerCfg))
        .pipe(gulp.dest(config.styles.dest))
        .pipe(reload({stream: true}))
        .pipe(notify({ // Add gulpif here
            title: 'Gulp',
            subtitle: 'success',
            message: 'Sass task',
            sound: "Pop"
        }));
});
